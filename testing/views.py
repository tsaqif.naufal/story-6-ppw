from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from .forms import StatusForm
from .models import Status
from datetime import datetime, timedelta

def index(request):
	context = {
		'form': StatusForm(),
		'status': Status.objects.all(),
	}

	return render(request, 'testing/landing.html', context)

def post_status(request):
	if request.method == 'POST':
		form = StatusForm(request.POST)
		if form.is_valid():
			content = form.cleaned_data['status']

			now = datetime.now() + timedelta(hours=7)
			day = now.strftime("%A")
			date = now.strftime("%d")
			month = now.strftime("%B")
			year = now.strftime("%Y")
			hour = now.strftime("%H")
			minute = now.strftime("%M")
			fulldate = f"{day}, {date} {month} {year}"
			time = f"{hour}:{minute} WIB"

			status = Status(date = fulldate, time = time, content = content)
			status.save()

	return HttpResponseRedirect(reverse('index'))