from django.test import TestCase
from django.urls import resolve
from .views import index
from .forms import StatusForm
from .models import Status
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class LandingPageTest(TestCase):
	def test_url_index_exists(self):
		response = self.client.get('/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_function(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_using_landing_page_template(self):
		response = self.client.get('/')
		self.assertTemplateUsed(response, 'testing/landing.html')

	def test_landing_page_content_is_written(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('Halo, apa kabar?', html_response)	

	def test_valid_form_input(self):
		form_data = {
			'status': 'Semangat ngerjain PPW nya!'
		}
		status_form = StatusForm(data = form_data)
		self.assertTrue(status_form.is_valid())

	def test_invalid_form_input(self):
		form_data = {
			'status': 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
		}
		status_form = StatusForm(data = form_data)
		self.assertFalse(status_form.is_valid())

	def test_form_has_csrf_token(self):
		response = self.client.get("/")
		self.assertContains(response, 'csrfmiddlewaretoken')

	def test_form_rendered_in_template(self):
		response = self.client.get("/")
		form = response.context["form"]
		self.assertIsInstance(form, StatusForm)

	def test_form_appears_in_template(self):
		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn('<textarea name="status" cols="40" rows="5" placeholder="What&#39;s new?" class="form-control" maxlength="300" required id="id_status">\n</textarea>', html_response)

	def test_url_post_status_exists(self):
		response = self.client.post("/post_status", data = {'status': 'Semangat ngerjain PPW nya!'})
		self.assertEqual(response.status_code, 302)

	def test_add_status_to_database(self):
		date = 'Sunday, 03 November 2019'
		time = '13:07'
		content = 'Semangat ngerjain PPW nya!'
		status = Status.objects.create(date = date, time = time, content = content)
		status.save()

	def test_valid_status(self):
		date = 'Sunday, 03 November 2019'
		time = '13:07'
		content = 'Semangat ngerjain PPW nya!'
		status = Status.objects.create(date = date, time = time, content = content)
		self.assertTrue(status.is_valid_status())

	def test_invalid_status(self):
		date = 'Sunday, 03 November 2019'
		time = '13:07'
		content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
		status = Status.objects.create(date = date, time = time, content = content)
		self.assertFalse(status.is_valid_status())

	def test_status_rendered_in_template(self):
		date = 'Sunday, 03 November 2019'
		time = '13:07'
		content = 'Semangat ngerjain PPW nya!'
		status = Status.objects.create(date = date, time = time, content = content)

		response = self.client.get("/")
		status = response.context["status"].get(id=1)
		self.assertIsInstance(status, Status)

	def test_status_appears_in_template(self):
		date = 'Sunday, 03 November 2019'
		time = '13:07'
		content = 'Semangat ngerjain PPW nya!'
		status = Status.objects.create(date = date, time = time, content = content)
		status.save()

		response = self.client.get("/")
		html_response = response.content.decode('utf8')
		self.assertIn(content, html_response)

	def test_status_string_representation(self):
		date = 'Sunday, 03 November 2019'
		time = '13:07'
		content = 'Semangat ngerjain PPW nya!'
		status = Status.objects.create(date = date, time = time, content = content)
		status.save()

		checked_status = Status.objects.get(id=1)
		self.assertEqual(str(checked_status), f"{checked_status.date} at {checked_status.time}")


class LandingPageFunctionalTest(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(LandingPageFunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(LandingPageFunctionalTest, self).tearDown()

	def test_input_status(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000')
		time.sleep(3)

		status_content = selenium.find_element_by_id('id_status')
		submit = selenium.find_element_by_id('button')

		status_content.send_keys('Coba Coba')
		time.sleep(3)

		submit.send_keys(Keys.RETURN)
		time.sleep(5)

		selenium.get('http://127.0.0.1:8000')
		assert 'Coba Coba' in selenium.page_source