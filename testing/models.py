from django.db import models

class Status(models.Model):
	date = models.CharField(max_length = 30)
	time = models.CharField(max_length = 5)
	content = models.CharField(max_length = 300)

	def is_valid_status(self):
		return len(self.content) > 0 and len(self.content) <= 300

	def __str__(self):
		return f"{self.date} at {self.time}"