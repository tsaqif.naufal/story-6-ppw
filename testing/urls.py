from django.urls import path
from . import views

urlpatterns = [
	path('', views.index, name='index'),
	path('post_status', views.post_status, name='post_status'),
]