from django import forms

class StatusForm(forms.Form):
	status_attrs = {
		'placeholder': "What's new?",
		'class': 'form-control',
		'rows': 5,
	}
	
	status = forms.CharField(max_length = 300, widget = forms.Textarea(attrs = status_attrs))